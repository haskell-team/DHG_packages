Source: darcs
Maintainer: Debian Haskell Group <pkg-haskell-maintainers@lists.alioth.debian.org>
Priority: optional
Section: haskell
Rules-Requires-Root: no
Build-Depends:
 haskell-devscripts (>= 0.13),
 bash-completion (>= 1:1.1),
 cdbs,
 debhelper (>= 10),
 ghc (>= 9.6),
 libghc-async-dev (>= 2.0.2),
 libghc-async-dev (<< 2.3),
 libghc-attoparsec-dev (>= 0.13.0.1),
 libghc-attoparsec-dev (<< 0.15),
 libghc-base16-bytestring-dev (>= 1.0),
 libghc-base16-bytestring-dev (<< 1.1),
 libghc-conduit-dev (>= 1.3.0),
 libghc-conduit-dev (<< 1.4),
 libghc-constraints-dev (>= 0.11),
 libghc-constraints-dev (<< 0.15),
 libghc-cryptonite-dev (>= 0.24),
 libghc-cryptonite-dev (<< 0.31),
 libghc-data-ordlist-dev (>= 0.4),
 libghc-data-ordlist-dev (<< 0.5),
 libghc-fgl-dev (>= 5.5.2.3),
 libghc-fgl-dev (<< 5.9),
 libghc-hashable-dev (>= 1.2.3.3),
 libghc-hashable-dev (<< 1.5),
 libghc-html-dev (>= 1.0.1.2),
 libghc-html-dev (<< 1.1),
 libghc-http-conduit-dev (>= 2.3),
 libghc-http-conduit-dev (<< 2.4),
 libghc-http-types-dev (>= 0.12.1),
 libghc-http-types-dev (<< 0.12.5),
 libghc-memory-dev (>= 0.14),
 libghc-memory-dev (<< 0.19),
 libghc-mmap-dev (>= 0.5.9),
 libghc-mmap-dev (<< 0.6),
 libghc-network-dev (>= 2.6),
 libghc-network-dev (<< 3.3),
 libghc-network-uri-dev (>= 2.6),
 libghc-network-uri-dev (<< 2.8),
 libghc-old-time-dev (>= 1.1.0.3),
 libghc-old-time-dev (<< 1.2),
 libghc-regex-applicative-dev (>= 0.2),
 libghc-regex-applicative-dev (<< 0.4),
 libghc-regex-base-dev (>= 0.94.0.1),
 libghc-regex-base-dev (<< 0.94.1),
 libghc-regex-tdfa-dev (>= 1.3.2),
 libghc-regex-tdfa-dev (<< 1.4),
 libghc-safe-dev (>= 0.3.20),
 libghc-safe-dev (<< 0.4),
 libghc-tar-dev (>= 0.5),
 libghc-tar-dev (<< 0.7),
 libghc-temporary-dev (>= 1.2.1),
 libghc-temporary-dev (<< 1.4),
 libghc-terminal-size-dev (>= 0.3.4),
 libghc-terminal-size-dev (<< 0.4),
 libghc-tls-dev (<< 2.0.0),
 libghc-unix-compat-dev (>= 0.6),
 libghc-unix-compat-dev (<< 0.8),
 libghc-utf8-string-dev (>= 1),
 libghc-utf8-string-dev (<< 1.1),
 libghc-vector-dev (>= 0.11),
 libghc-vector-dev (<< 0.14),
 libghc-zip-archive-dev (>= 0.3),
 libghc-zip-archive-dev (<< 0.5),
 libghc-zlib-dev (>= 0.6.1.2),
 libghc-zlib-dev (<< 0.8),
Standards-Version: 4.7.0
Homepage: http://darcs.net/
Vcs-Browser: https://salsa.debian.org/haskell-team/DHG_packages/tree/master/p/darcs
Vcs-Git: https://salsa.debian.org/haskell-team/DHG_packages.git [p/darcs]

Package: darcs
Architecture: any
Multi-Arch: foreign
Section: vcs
Depends:
 ${misc:Depends},
 ${shlibs:Depends},
Description: distributed, interactive, smart revision control system
 Darcs is a free, open source revision control system.  It is:
 .
  * Distributed: Every user has access to the full command set,
    removing boundaries between server and client or committer and
    non-committers.
  * Interactive: Darcs is easy to learn and efficient to use because
    it asks you questions in response to simple commands, giving you
    choices in your work flow. You can choose to record one change in
    a file, while ignoring another. As you update from upstream, you
    can review each patch name, even the full "diff" for interesting
    patches.
  * Smart: Originally developed by physicist David Roundy, darcs is
    based on a unique algebra of patches. This smartness lets you
    respond to changing demands in ways that would otherwise not be
    possible.
