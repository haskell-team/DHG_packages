Index: b/unicode-data.cabal
===================================================================
--- a/unicode-data.cabal
+++ b/unicode-data.cabal
@@ -1,6 +1,7 @@
 cabal-version:       2.2
 name:                unicode-data
 version:             0.4.0.1
+x-revision: 3
 synopsis:            Access Unicode Character Database (UCD)
 description:
   @unicode-data@ provides Haskell APIs to efficiently access the
@@ -29,6 +30,7 @@ tested-with:         GHC==8.0.2
                    , GHC==9.0.1
                    , GHC==9.2.1
                    , GHC==9.4.2
+                   , GHC==9.6.0
 
 extra-source-files:
     Changelog.md
@@ -115,7 +117,7 @@ library
   hs-source-dirs: lib
   ghc-options: -O2
   build-depends:
-      base >=4.7 && <4.18
+      base >=4.7 && <4.21
 
 executable ucd2haskell
   import: default-extensions, compile-options
@@ -127,7 +129,7 @@ executable ucd2haskell
   if flag(ucd2haskell)
     buildable: True
     build-depends:
-        base             >= 4.7   && < 4.18
+        base             >= 4.7   && < 4.20
       , containers       >= 0.5   && < 0.7
       , directory        >= 1.3.6 && < 1.3.9
       , filepath         >= 1.4.100 && < 1.5
@@ -137,6 +139,9 @@ executable ucd2haskell
     buildable: False
 
 test-suite test
+  -- Testsuite is broken with GHC 9.6,
+  -- see https://github.com/composewell/unicode-data/issues/118
+  buildable: False
   import: default-extensions, compile-options
   type: exitcode-stdio-1.0
   main-is: Main.hs
@@ -145,8 +150,8 @@ test-suite test
   other-modules:
       Unicode.CharSpec
   build-depends:
-      base             >= 4.7   && < 4.18
-    , hspec            >= 2.0   && < 2.11
+      base             >= 4.7   && < 4.20
+    , hspec            >= 2.0   && < 2.12
     , unicode-data
   -- We need to match a GHC version with the same Unicode version.
   -- See: test/Unicode/CharSpec.hs for compatibility table.
@@ -160,8 +165,8 @@ benchmark bench
   hs-source-dirs: bench
   main-is: Main.hs
   build-depends:
-    base        >= 4.7   && < 4.18,
-    deepseq     >= 1.1   && < 1.5,
+    base        >= 4.7   && < 4.20,
+    deepseq     >= 1.1   && < 1.6,
     tasty-bench >= 0.2.5 && < 0.4,
     tasty       >= 1.4.1,
     unicode-data
