haskell-pandoc (3.1.11.1-3) unstable; urgency=medium

  * Fix tests to work with new commonmark-pandoc

 -- Ilias Tsitsimpis <iliastsi@debian.org>  Wed, 23 Oct 2024 23:25:35 +0300

haskell-pandoc (3.1.11.1-2) unstable; urgency=medium

  * Fix FTBFS due to test failures

 -- Scott Talbert <swt@techie.net>  Mon, 14 Oct 2024 10:23:33 -0400

haskell-pandoc (3.1.11.1-1) unstable; urgency=medium

  [ Ilias Tsitsimpis ]
  * Declare compliance with Debian policy 4.7.0

  [ Scott Talbert ]
  * New upstream release (Closes: #1053777, #1062045)

 -- Scott Talbert <swt@techie.net>  Tue, 08 Oct 2024 16:40:26 -0400

haskell-pandoc (3.1.3-3) unstable; urgency=medium

  [ zhangdandan <zhangdandan@loongson.cn> ]
  * Use extra --ghc-options and enable tests in d/rules on loong64 (Closes:
    #1071169)

  [ Scott Talbert ]
  * Use Python 3 for Python filters (Closes: #1077793)
  * Fix memory leak when converting markdown files (Closes: #1078342)

 -- Scott Talbert <swt@techie.net>  Fri, 06 Sep 2024 22:04:41 -0400

haskell-pandoc (3.1.3-2) unstable; urgency=medium

  * Run build target twice, as a workaround for a GHC bug (Closes: #1070015)
  * Disable tests on loong64 (Closes: #1069165)

 -- Ilias Tsitsimpis <iliastsi@debian.org>  Wed, 01 May 2024 12:36:13 +0300

haskell-pandoc (3.1.3-1) unstable; urgency=medium

  * New upstream release (Closes: #1057852)

 -- Scott Talbert <swt@techie.net>  Tue, 02 Jan 2024 22:56:34 -0500

haskell-pandoc (3.0.1-4) unstable; urgency=medium

  [ Jonas Smedegaard ]
  * revive and unfuzz patches lost in transition from src:pandoc:
    + 2001: avoid potential privacy breaches in templates
      closes: bug#1059146
    + 2002: improve error message when pdf program is missing

 -- Ilias Tsitsimpis <iliastsi@debian.org>  Fri, 22 Dec 2023 10:28:46 +0200

haskell-pandoc (3.0.1-3) unstable; urgency=medium

  * Apply upstream patch to fix FTBFS on 32-bit platforms

 -- Ilias Tsitsimpis <iliastsi@debian.org>  Wed, 06 Dec 2023 18:21:57 +0200

haskell-pandoc (3.0.1-2) unstable; urgency=medium

  * Fix CVE-2023-35936

 -- Ilias Tsitsimpis <iliastsi@debian.org>  Fri, 01 Dec 2023 14:08:08 +0200

haskell-pandoc (3.0.1-1) unstable; urgency=low

  * Initial release

 -- Scott Talbert <swt@techie.net>  Sat, 25 Nov 2023 11:58:14 -0500
