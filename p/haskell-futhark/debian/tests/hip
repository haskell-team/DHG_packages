#!/bin/sh
# Adapted from autopkgtest driver for ROCm

if [ ! -e /dev/kfd ]
then
        echo "/dev/kfd not present, system either lacks AMD GPU or AMDGPU driver is not loaded."
        echo "Skipping tests."
        # Magic number to signal 'skipped'
        exit 77
elif [ "$(id -u)" != "0" ] && [ ! -r /dev/kfd ]
then
        echo "/dev/kfd present but no read permission."
        echo "Skipping tests."
        exit 77
fi

# First, gather system info
sudo -n mount -t debugfs none /sys/kernel/debug || true
if sudo -n [ -d /sys/kernel/debug/dri ]
then
        for index in $(sudo -n ls /sys/kernel/debug/dri)
        do
                info="/sys/kernel/debug/dri/$index/amdgpu_firmware_info"
                if sudo -n [ -f "$info" ]
                then
                        # shellcheck disable=SC2024   # we don't need privileged write
                        sudo -n cat "$info" > "$AUTOPKGTEST_ARTIFACTS/amdgpu_firmware_info.$index"
                fi
        done
else
        echo "Could not read /sys/kernel/debug/dri" >> "$AUTOPKGTEST_ARTIFACTS/firmware.err"
fi
# shellcheck disable=SC2024   # we don't need privileged write
sudo -n dmesg > "$AUTOPKGTEST_ARTIFACTS/dmesg.before" || true
# shellcheck disable=SC2024   # we don't need privileged write
sudo rocminfo > "$AUTOPKGTEST_ARTIFACTS/rocminfo.txt" || true

# Any individual failure is overall failure
EXITCODE=0
cp -r tests $AUTOPKGTEST_TMP

futhark test --backend=hip --notty $AUTOPKGTEST_TMP/tests || EXITCODE=1

# Tests might have generated new messages
# shellcheck disable=SC2024   # we don't need privileged write
sudo -n dmesg > "$AUTOPKGTEST_ARTIFACTS/dmesg.after" || true

exit $EXITCODE
